#include <Stepper_28BYJ_48.h>
#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>

#define FIREBASE_HOST "iot-smarthouse.firebaseio.com"
#define FIREBASE_AUTH "wVAvI1KQT8RcOfr4unUMMkMvZjwrC3EQiBeoqLUB"
#define WIFI_SSID "IosEmerson" 
#define WIFI_PASSWORD "mysong2018"



int portaLDR = A0;

Stepper_28BYJ_48 stepper(D0,D1,D2,D3);

void setupWifi(){
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());
}
void setupFirebase(){
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  
  Firebase.setInt("LDRvalor", 0);
}

 

void setup()   
{  
    Serial.begin(9600);
    setupWifi();   
    setupFirebase();
}  

bool a = false;

void loop()  
{  
  int estado = analogRead(portaLDR);
  Firebase.pushInt("luminosidade", estado);
  
  Serial.println(estado);
  Serial.println(a);
  delay(3000);
    
  if (estado > 920 && a== false)
  {
   stepper.step(1240); 
   Firebase.pushInt("Janela aberta", estado);
   a = true; 
   Firebase.setBool("Estado da Janela", a); //false -- fechado, true --- aberta
   delay(500);
   
    
  }  

  if (estado < 500 && a== true)  
  {  
    stepper.step(-1240);  
    Firebase.pushInt("Janela fechada", estado);
    a = false;
    Firebase.setBool("Estado da Janela", a); //false -- fechado, true --- aberta
    delay(500);
   

  } 

}  



(function(){
    
  // Inicia o Firebase
  var config = {
    apiKey: "AIzaSyAOx7TT0fBKyyAgvqVW_ajI0qZiUNM0hNA",
    authDomain: "iot-smarthouse.firebaseapp.com",
    databaseURL: "https://iot-smarthouse.firebaseio.com",
    projectId: "iot-smarthouse",
    storageBucket: "iot-smarthouse.appspot.com",
    messagingSenderId: "380985235736"
    };
  firebase.initializeApp(config);

  var db = firebase.database();

  // Leitura dos dados no firebase
  
  var lumiRef = db.ref('luminosidade');
  
  // Registra as funções que atualizam os gráficos e dados atuais da telemetria
  
  lumiRef.on('value', onNewData('currentlumi', 'lumiLineChart' , 'Luminosidade - LDR', 'LDR'));

})();


// Retorna uma função de acordo com os dados atualizados
// Atualiza o valor atual do elemento, com os parametros definidos (currentValueEl e metric)
// e monta o gráfico com os dados e descrição do tipo de dados (chartEl, label)
function onNewData(currentValueEl, chartEl, label, metric){
  return function(snapshot){
    var readings = snapshot.val();
    if(readings){
        var currentValue;
        var data = [];
        for(var key in readings){
          currentValue = readings[key]
          data.push(currentValue);
        }

        document.getElementById(currentValueEl).innerText = currentValue + ' ' + metric;
        buildLineChart(chartEl, label, data);
    }
  }
}

// Constroi um gráfico de linha no elemento (el) com a descrição (label) e os
// dados passados (data)
function buildLineChart(el, label, data){
  var elNode = document.getElementById(el);
  new Chart(elNode, {
    type: 'line',
    data: {
        labels: new Array(data.length).fill(""),
        datasets: [{
            label: label,
            data: data,
            borderWidth: 1,
            fill: false,
            spanGaps: false,
            lineTension: 0.1,
            backgroundColor: "#F9A825",
            borderColor: "#F9A825"
        }]
    }
  });
}
